package com.exam.servlce.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.servlce.entity.TUser;
import com.exam.servlce.mapper.TUserMapper;
import com.exam.servlce.service.TUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import entity.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author guo
 * @since 2020-11-17
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements TUserService {

    @Override
    public List<TUser> AllTUser() {
        return baseMapper.selectList(null);
    }

    @Override
    public boolean DeleteUser(Integer id) {

        return this.removeById(id);
    }

    @Override
    public boolean saveUser(TUser tUser) {

        if (tUser.equals(null)){
            return false;
        }

        return  this.save(tUser);
    }


}
