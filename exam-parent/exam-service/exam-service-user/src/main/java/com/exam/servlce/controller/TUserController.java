package com.exam.servlce.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.api.R;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.servlce.entity.TUser;
import com.exam.servlce.service.TUserService;
import entity.Result;
import entity.StatusCode;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author guo
 * @since 2020-11-17
 */
@RestController
@RequestMapping("/servlce/t-user")
@CrossOrigin //解决跨域问题 1.协议问题
@Api(tags = "用户管理")
public class TUserController {

    @Autowired
    private TUserService tUserService;


    @ResponseBody
    @ApiOperation(value = "查询所有用户信息", notes = "查询所有用户信息")
    @GetMapping(value = "/AllUser",produces = { "application/json;charset=UTF-8"})
    public Result<List<TUser>> AllUser(){
        List<TUser> tUsers = tUserService.AllTUser();
        return new Result(true, StatusCode.OK,"成功",tUsers);
    }

    @ApiOperation("分页查询不带条件")
    @ApiImplicitParams({

            @ApiImplicitParam(name = "limit", defaultValue = "1", paramType = "query", required = true),
            @ApiImplicitParam(name = "current", defaultValue = "20", paramType = "query", required = true),

    })
    @ResponseBody
    @GetMapping(value = "/pageUserList",produces = { "application/json;charset=UTF-8"})
    public Result pageUserList(TUser tuser,
                               @RequestParam(value = "limit", defaultValue = "1") int limit,
                               @RequestParam(value = "current", defaultValue = "20") int current){


        IPage<TUser> tUserPage = new Page<>(limit,current);
        QueryWrapper<TUser> wrapper = new QueryWrapper<>();
        if (StringUtils.isNotBlank(tuser.getUsername())){
            wrapper.eq(tuser.USERNAME,tuser.getUsername());
            System.out.println(tuser.getUsername());
        }
        if (StringUtils.isNotBlank(tuser.getPhone())){
            wrapper.eq(tuser.PHONE,tuser.getPhone());
        }
        if (StringUtils.isNotBlank(tuser.getQq())){
            wrapper.eq(tuser.QQ,tuser.getPhone());
        }

        tUserService.page(tUserPage,wrapper);
        System.out.println(tUserPage);
        return new Result(true,StatusCode.OK,"成功",tUserPage);
    }
    @ApiOperation(value = "删除用户", notes = "删除用户")
    @DeleteMapping("delet/{id}")
    public Result deleteBatch(@PathVariable Integer id){


        boolean b = tUserService.DeleteUser(id);
        if (b == false){
            return new Result(false,StatusCode.ERROR,"删除失败",null);
        }
        return new Result(true,StatusCode.OK,"删除成功",null);

    }

    @ApiOperation(value = "添加用户", notes = "添加用户")
    @PostMapping("/save")
    public Result save(@RequestBody TUser user){

        boolean b = tUserService.saveUser(user);
        if (b == false){
            return new Result(false,StatusCode.ERROR,"添加失败",null);
        }
        return new Result(true,StatusCode.OK,"添加成功",null);
    }

    @ApiOperation(value = "修改用户", notes = "修改用户")
    @PostMapping("/update")
    public Result update(@RequestBody TUser user){

        boolean b = tUserService.updateById(user);
        if (b == false){
            return new Result(false,StatusCode.ERROR,"修改失败",null);
        }
        return new Result(true,StatusCode.OK,"修改成功",null);

    }

    @ApiOperation(value = "更改用户状态", notes = "用户状态 0 启用 1 禁用")
    @PostMapping("/changeStatus")
    public Result upadataState(@RequestBody TUser tUser){

        TUser tUsers = tUserService.getById(tUser.getId());

        if (tUsers == null){
            return new Result(false,StatusCode.ERROR,"用户不存在",null);
        }
        Integer status = tUser.getState();

        tUser.setState(status);
        tUserService.updateById(tUser);
        return new Result(true,StatusCode.OK,"修改成功",null);

    }


}

