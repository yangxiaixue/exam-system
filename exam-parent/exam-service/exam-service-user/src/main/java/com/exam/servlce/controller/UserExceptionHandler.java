package com.exam.servlce.controller;


import entity.Result;
import entity.StatusCode;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice

public class UserExceptionHandler {
    /**
     * 异常处理
     */

    public Result error(Exception e){
        e.printStackTrace();
        return new Result(true, StatusCode.ERROR,e.getMessage());
    }

}
