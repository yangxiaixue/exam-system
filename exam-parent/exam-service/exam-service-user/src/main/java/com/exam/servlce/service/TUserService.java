package com.exam.servlce.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.exam.servlce.entity.TUser;
import com.baomidou.mybatisplus.extension.service.IService;
import entity.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author guo
 * @since 2020-11-17
 */
public interface TUserService extends IService<TUser> {

    //查询全部用户
    List<TUser> AllTUser();

    //删除用户
    boolean DeleteUser(Integer id);

    //添加用户
    boolean saveUser(TUser tUser);



}
