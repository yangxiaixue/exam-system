# 在线考试系统 

#### 介绍
利用spring cloud 、 spring boot 、 Mybatis - Plus 在线考试系统
系统的主要内容是实现智能化考试，教师可以进行管理试题，管理试卷，发布试卷，成绩统计可视化等操作。其中出题方式有两种，自动出题和手动出题。学生在参加完考试之后，系统会自动进行阅卷，并给出学生的考试成绩。学生可以实时看到自己的成绩，成绩可通过可视化图表进行展示。

#### 软件架构



#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
